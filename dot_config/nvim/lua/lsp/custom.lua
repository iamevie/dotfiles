local lspconfig = require 'lspconfig'

lspconfig.clangd.setup {
	cmd = { 'clangd', '-header-insertion=never' },
}

lspconfig.rust_analyzer.setup {}
lspconfig.lua_ls.setup {}
lspconfig.asm_lsp.setup {}
lspconfig.html.setup {}
lspconfig.tsserver.setup {}
lspconfig.cssls.setup {}
lspconfig.svelte.setup {}
