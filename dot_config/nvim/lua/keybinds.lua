local function bind(mode, key, cmd)
	vim.api.nvim_set_keymap(mode, key, cmd, { noremap = true, silent = true })
end

-- open file under cursor not in terminal, when in terminal mode
-- bind("n", "<Leader>gf", '<Cmd>let mycurf=expand("<cfile>")<Cr><C-w> h <Cmd>execute("e ".mycurf)<Cr>')
-- wanted to use cword instead of cfile, as it should be possible to jump to the location marked after the name by tools such as cargo, but in just opens a file fith the appended numbers
vim.cmd('nnoremap <silent> <Leader>gf :let mycurf=expand("<cfile>")<cr><c-w>p:execute("e ".mycurf)<cr>')

-- open man file of cword in new tab
bind("n", "<Leader>m", "<Cmd>Man<Cr><C-w>T")

-- undo & redo
bind("n", "u", "<Cmd>undo<Cr>")
bind("n", "<S-u>", "<Cmd>redo<Cr>")

-- better paste
bind("v", "p", '"_dP')

bind("n", "<Leader>f", "<Cmd>Telescope find_files<Cr>")
bind("n", "<Leader>b", "<Cmd>Telescope buffers<Cr>")
bind("n", "gr", "<Cmd>Telescope lsp_references<Cr>")

-- outlines
bind("n", "<Leader>o", "<Cmd>Telescope lsp_document_symbols<Cr>")

-- quick fixes
-- add word under cursor to quick list
bind("n", "<Leader>*", "<Cmd>lgrep -r <cword> ./<Cr><Cmd>lopen<Cr>")

-- terminal mode
bind("n", "<Leader>t", "<Cmd>vs | terminal<Cr><Cmd>vertical resize 90<Cr>i")
bind("t", "<C-[>", "<C-\\><C-n>")


vim.api.nvim_create_autocmd('LspAttach', {
	group = vim.api.nvim_create_augroup('UserLspConfig', {}),
	callback = function(ev)
		local function lsp_bind(key, cmd)
			vim.keymap.set("n", key, cmd, { noremap = true, silent = true, buffer = ev.buf })
		end
		vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

		lsp_bind("gD", "<Cmd>lua vim.lsp.buf.declaration()<CR>")
		lsp_bind("gd", "<Cmd>lua vim.lsp.buf.definition()<CR>")
		lsp_bind("gi", "<Cmd>lua vim.lsp.buf.implementation()<CR>")
		-- lsp_bind("gr", "<Cmd>lua vim.lsp.buf.references()<CR>")
		lsp_bind("<Space>r", "<Cmd>lua vim.lsp.buf.rename()<CR>")
		lsp_bind("<Space>a", "<Cmd>lua vim.lsp.buf.code_action()<CR>")
	end
})
