return {
	{'folke/which-key.nvim', config = function()
		local wk = require 'which-key'
		wk.setup()
	end},

	 {'nvim-telescope/telescope.nvim', dependencies = {
	 	'nvim-lua/plenary.nvim',
	 	{ 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' }
	 }, config = function()
	 	require 'user.telescope'
	 end},

	{'neovim/nvim-lspconfig'},

	{'williamboman/mason.nvim', dependencies = {
		'williamboman/mason-lspconfig.nvim'
	}, build = ':MasonUpdate'},

	{'hrsh7th/nvim-cmp', dependencies = {
		'onsails/lspkind.nvim',
		'hrsh7th/cmp-buffer',
		'hrsh7th/cmp-nvim-lua',
		'hrsh7th/cmp-nvim-lsp',
		'saadparwaiz1/cmp_luasnip',
		{'L3MON4D3/LuaSnip', dependencies = {
			'rafamadriz/friendly-snippets'
		}}
	}, config = function()
		require 'user.cmp'
	end},

	{'windwp/nvim-autopairs', config = function()
		require 'nvim-autopairs'.setup()
	end},

	{'mg979/vim-visual-multi'},

	{ 'stevearc/stickybuf.nvim', config = function()
		require 'user.stickybuf'
	end},

	{'nvim-treesitter/nvim-treesitter', config = function()
		require 'nvim-treesitter.configs'.setup {
			highlight = true
		}
	end},

	{'blazkowolf/gruber-darker.nvim'},
	{'RaafatTurki/hex.nvim', config = function()
		require 'user.hex'
	end},

	{'willothy/flatten.nvim', config = function()
		require 'user.flatten'
	end, lazy = false, priority = 1001},

	{'tamton-aquib/staline.nvim', config = function()
		require 'user.staline'
	end}
}
