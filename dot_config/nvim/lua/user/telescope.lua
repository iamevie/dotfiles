local telescope = require 'telescope'
local actions = require 'telescope.actions'

telescope.setup {
	defaults = {
		mappings = {
			i = {
				['<C-c>'] = actions.close
			},
			n = {
				['<C-c>'] = actions.close
			}
		},
		file_ignore_patterns = {
			"target/",
			"build/",
			"bin/"
		},
		initial_mode = "insert",
		selection_strategy = "reset",
		sorting_strategy = "ascending",
		layout_strategy = "horizontal",
		layout_config = {
			horizontal = {
				prompt_position = "top",
				preview_width = 0.6,
				results_width = 0.4,
			},
			vertical = {
				mirror = false,
			},
			width = 0.6,
			height = 0.80,
			preview_cutoff = 120,
		},
		borderchars = { " ", " ", " ", " ", " ", " ", " ", " " },
    	set_env = { ["COLORTERM"] = "truecolor" }, -- default = nil,
	},
	pickers = {
		lsp_references = {
			theme = "cursor"
		},
	},
	extensions = {
		fzf = {
			fuzzy = true,
			override_generic_sorter = true,
			override_file_sorter = true,
			case_mode = 'smart_case'
		}
	}
}

telescope.load_extension('fzf')
