local symbols = require 'symbols-outline'

local opts = {
	highlight_hovered_item = true,
	auto_close = true,
	auto_preview = true,
	show_number = false,
	show_relative_numbers = true,
	auto_unfold_hover = true,
	width = 40,
}

symbols.setup(opts)
