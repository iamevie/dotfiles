vim.g.mapleader = ' '
vim.loader.enable()

local lazypath = vim.fn.stdpath('data') .. 'lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		'git',
		'clone',
		'https://github.com/folke/lazy.nvim.git',
		lazypath,
	})
end

vim.opt.rtp:prepend(lazypath)

require 'options'
require 'keybinds'
require 'lazy'.setup(require 'plugins')
require 'lsp'

vim.cmd 'colorscheme gruber-darker'
vim.cmd 'hi Normal guibg=NONE ctermbg=NONE'
vim.cmd 'hi NormalSB guibg=NONE ctermbg=NONE'
vim.cmd 'hi NormalNC guibg=NONE ctermbg=NONE'

vim.cmd('hi link TelescopeNormal NormalFloat')
vim.cmd('hi link TelescopeBorder NormalFloat')
vim.cmd('hi link TelescopePreviewNormal NormalFloat')
vim.cmd('hi link TelescopeResultsNormal NormalFloat')

-- global status bar
vim.api.nvim_create_autocmd('BufEnter', {
	callback = function()
		vim.cmd [[ set laststatus=3 ]]
		vim.cmd [[ highlight WinSeparator guibg=NONE ]]
	end
})

