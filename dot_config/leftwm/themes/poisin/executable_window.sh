#!/usr/bin/env bash
dir="$HOME/.config/leftwm/themes/current"
theme='style-5'

## Run
rofi \
    -show window \
    -theme ${dir}/${theme}.rasi

